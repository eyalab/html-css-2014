/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function delErr(event){
   // alert(event.target());
  // alert(formElement.pass.length);
  //alert(event.toString());
    var t0 = $.trim($('#nameReg').val());
    if (t0 !== "") {                    
        $('#errorNameReg').html("");
    }
    var t1 = $.trim($('#emailReg').val());
    if (t1 !== "") {                    
        $('#erroreMailReg').html("");
    }
    var t0 = $.trim($('#userReg').val());
    if (t0 !== "") {                    
        $('#errorUserReg').html("");
    }
    var t0 = $.trim($('#passwordReg').val());
    if (t0 !== "" ) {                    
        $('#errorPasswordReg').html("");
        
    }
}
            
function validate() {
    var errorOccured = false;
    var t0 = $.trim($('#nameReg').val());
    if (t0 === "") {                    
        $('#errorNameReg').html("Please enter your name!");
        errorOccured = true;                    
    }
    if (t0 !== "") {                    
        $('#errorNameReg').html("");
    }

    var t1 = $.trim($('#emailReg').val());
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var vaildEmail = false;
    
    if(filter.test(t1) === false){
        $('#erroreMailReg').html("invalid email, legal form is: \"email@server.end\"");
        errorOccured = true;
    }
    else{
        vaildEmail = true;
    }
    if (t1 === "") {                    
        $('#erroreMailReg').html("Please enter your email adress!");
        errorOccured = true;
    }
    if (t1 !== "" && vaildEmail) {                    
        $('#erroreMailReg').html("");                    
    }

    var t2 = $.trim($('#userReg').val());
    if (t2 === "") {                    
        $('#errorUserReg').html("Please enter your user name!");
        errorOccured = true;
    }
    if (t2 !== "") {                    
        $('#errorUserReg').html("");
    }

    var t3 = $.trim($('#passwordReg').val());
    var alphanumericFilter = /^[a-zA-Z0-9]+$/;
    
    
    if (t3 === "") {                    
        $('#errorPasswordReg').html("Please enter your password!");
        errorOccured = true;
    }
    if (t3.length < 8 && t3 !== "") {                    
        $('#errorPasswordReg').html("Please enter 8, or more, digits password!");
        errorOccured = true;
    }
    if(alphanumericFilter.test(t3)===false && t3.length >=8 ){
        $('#errorPasswordReg').html("Please use only digits and numbers");
        errorOccured = true;
    }
    
    if(t3)
    if (t3.length>=8 && t3 !== "" && alphanumericFilter.test(t3)!==false) {                    
        $('#errorPasswordReg').html("");
    }

    if (errorOccured === true){
        return false;
    }
    var arr = $.map($('input[name="op[]"]:checked'), function(e,i) { return +e.value; });
    console.log(arr);
    return true;
}